var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
  lineNumbers: true,
  mode: "javascript",
  theme: "cool-glow"
});

// Get the repoName from the URL
var urlParams = new URLSearchParams(window.location.search);
var repoName = urlParams.get('repoName') || 'default-repo';

// Load file system from local storage for the specific repo
var fileSystem = JSON.parse(localStorage.getItem(repoName)) || {};

// Save file system to local storage for the specific repo
function saveToLocalStorage() {
  localStorage.setItem(repoName, JSON.stringify(fileSystem));
}

// Function to save file
function saveFile() {
  var filename = document.getElementById("filename").value;
  var code = editor.getValue();

  if (filename) {
    var path = filename.split('/');
    var file = path.pop();
    var current = fileSystem;

    // Create folders if they don't exist
    path.forEach(function(folder) {
      if (!current[folder]) {
        current[folder] = {};
      }
      current = current[folder];
    });

    // Save the file
    current[file] = code;

    // Update the file structure display and save to local storage
    displayFileStructure();
    saveToLocalStorage();
  } else {
    alert("Please enter a file name.");
  }
}

// Function to delete file or folder
function deleteItem(pathArray) {
  var file = pathArray.pop();
  var current = fileSystem;

  for (var i = 0; i < pathArray.length; i++) {
    if (!current[pathArray[i]]) {
      alert("Item not found.");
      return;
    }
    current = current[pathArray[i]];
  }

  if (current[file]) {
    // Show confirmation dialog before deleting
    if (confirm("Are you sure you want to delete '" + file + "'?")) {
      delete current[file];
      displayFileStructure();
      saveToLocalStorage();
    }
  } else {
    alert("Item not found.");
  }
}

// Function to delete entire file system
function deleteFileSystem() {
  if (confirm("Are you sure you want to delete the entire file system?")) {
    fileSystem = {};
    displayFileStructure();
    saveToLocalStorage();
  }
}

// Function to preview file
function previewFile(filename, content) {
  document.getElementById("filename").value = filename;
  editor.setValue(content);
  var fileContents = document.getElementById("file-contents");
  fileContents.textContent = content;
  fileContents.style.display = 'block';
}

// Function to toggle folder visibility
function toggleFolder(event) {
  event.stopPropagation();
  var folderElement = event.currentTarget;
  var ul = folderElement.querySelector('ul');
  if (ul.style.display === 'none' || ul.style.display === '') {
    ul.style.display = 'block';
  } else {
    ul.style.display = 'none';
  }
}

// Function to display the file structure
function displayFileStructure() {
  var container = document.getElementById('file-structure');
  container.innerHTML = '';

  function renderFileStructure(obj, parentElement, path = '') {
    var ul = document.createElement('ul');

    // Separate folders and files
    var folders = [];
    var files = [];
    for (var key in obj) {
      if (typeof obj[key] === 'object') {
        folders.push(key);
      } else {
        files.push(key);
      }
    }

    // Render folders
    folders.forEach(function(folder) {
      var li = document.createElement('li');
      li.classList.add('folder');
      var icon = document.createElement('i');
      icon.classList.add('fas', 'fa-folder');
      var deleteButton = document.createElement('i');
      deleteButton.classList.add('fas', 'fa-trash-alt', 'delete-button');
      var trashBin = document.createElement('i');
      trashBin.classList.add('fas', 'fa-trash', 'trash-bin');
      var buttonContainer = document.createElement('div'); // Container for icons
      buttonContainer.appendChild(icon);
      buttonContainer.appendChild(deleteButton);
      li.appendChild(buttonContainer); // Append container
      li.appendChild(document.createTextNode(' ')); // Add space
      var span = document.createElement('span');
      span.textContent = folder;
      li.appendChild(span);
      li.appendChild(trashBin); // Append trash bin
      deleteButton.addEventListener('click', function(event) {
        event.stopPropagation();
        deleteItem((path + folder).split('/'));
      });
      trashBin.addEventListener('click', function(event) {
        event.stopPropagation();
        deleteItem((path + folder).split('/'));
      });
      li.addEventListener('click', toggleFolder);
      var subUl = document.createElement('ul');
      subUl.style.display = 'none';
      renderFileStructure(obj[folder], subUl, path + folder + '/');
      li.appendChild(subUl);
      ul.appendChild(li);
    });

    // Render files
    files.forEach(function(file) {
      var li = document.createElement('li');
      li.classList.add('file');
      var icon = document.createElement('i');
      icon.classList.add('fas', 'fa-file');
      var deleteButton = document.createElement('i');
      deleteButton.classList.add('fas', 'fa-trash-alt', 'delete-button');
      var trashBin = document.createElement('i');
      trashBin.classList.add('fas', 'fa-trash', 'trash-bin');
      var buttonContainer = document.createElement('div'); // Container for icons
      buttonContainer.appendChild(icon);
      buttonContainer.appendChild(deleteButton);
      li.appendChild(buttonContainer); // Append container
      li.appendChild(document.createTextNode(' ')); // Add space
      var span = document.createElement('span');
      span.textContent = file;
      li.appendChild(span);
      li.appendChild(trashBin); // Append trash bin
      deleteButton.addEventListener('click', function(event) {
        event.stopPropagation();
        deleteItem((path + file).split('/'));
      });
      trashBin.addEventListener('click', function(event) {
        event.stopPropagation();
        deleteItem((path + file).split('/'));
      });
      li.addEventListener('click', function() {
        previewFile(path + file, obj[file]);
      });
      ul.appendChild(li);
    });

    parentElement.appendChild(ul);
  }

  renderFileStructure(fileSystem, container);
}

// Display the file structure on page load
window.onload = displayFileStructure;

function createZip() {
  var zip = new JSZip();

  // Recursively add files and folders to the zip
  function addFilesToZip(files, parentPath) {
    for (var key in files) {
      if (typeof files[key] === 'object') {
        // Create folder in the zip
        var folder = zip.folder(key);
        // Recursively add files and folders inside this folder
        addFilesToZip(files[key], parentPath ? parentPath + '/' + key : key);
      } else {
        // Add file to the zip
        zip.file(parentPath ? parentPath + '/' + key : key, files[key]);
      }
    }
  }

  // Add file system contents to the zip
  addFilesToZip(fileSystem, '');

  // Generate the zip file asynchronously
  zip.generateAsync({ type: 'blob' })
    .then(function(content) {
      // Create a download link for the zip file
      var a = document.createElement('a');
      var url = URL.createObjectURL(content);
      a.href = url;
      a.download = repoName + '.zip'; // Use repoName as the zip file name
      a.click();
      URL.revokeObjectURL(url);
    })
    .catch(function (error) {
      alert('Error generating zip file:', error);
    });
}
