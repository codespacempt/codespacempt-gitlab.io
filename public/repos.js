// Function to add a repository to the list with a hidden delete button
function addRepoToList(repoName) {
    var ul = document.getElementById('folders');
    var li = document.createElement('li');
    var a = document.createElement('a');
    a.textContent = repoName;
    a.href = '/repositories/repos?repoName=' + encodeURIComponent(repoName);
    a.target = "_blank"; // Open in a new tab
    li.appendChild(a);

    // Create a delete button for each repository and hide it
    var deleteButton = document.createElement('button');
    deleteButton.textContent = 'Delete';
    deleteButton.style.display = 'none'; // Hide the button by default
    deleteButton.onclick = function() {
        if (confirm('Are you sure you want to delete this repository?')) {
            li.remove();
            deleteRepo(repoName);
        }
    };
    li.appendChild(deleteButton);

    ul.appendChild(li);
}

// Function to save a new repository to localStorage
function saveRepo(repoName) {
    var repos = JSON.parse(localStorage.getItem('repos')) || [];
    repos.push(repoName);
    localStorage.setItem('repos', JSON.stringify(repos));
}

// Function to delete a repository from localStorage
function deleteRepo(repoName) {
    var repos = JSON.parse(localStorage.getItem('repos')) || [];
    var index = repos.indexOf(repoName);
    if (index !== -1) {
        repos.splice(index, 1);
        localStorage.setItem('repos', JSON.stringify(repos));
    }
}

// Function to create a new repository and add it to the list
function createRepo() {
    var repoName = document.getElementById('repoName').value;
    if (repoName) {
        addRepoToList(repoName);
        saveRepo(repoName);
    } else {
        alert('Please enter a repository name.');
    }
}

// Function to load repositories from localStorage
function loadRepos() {
    var repos = JSON.parse(localStorage.getItem('repos')) || [];
    repos.forEach(addRepoToList);
}

// Load repositories when the page loads
window.addEventListener('DOMContentLoaded', loadRepos);

// Function to toggle the visibility of delete buttons
function toggleDeleteButtons() {
    var deleteButtons = document.querySelectorAll('#folders li button');
    deleteButtons.forEach(function(button) {
        button.style.display = button.style.display === 'none' ? 'inline' : 'none';
    });
}

// Add event listener to the delete toggle checkbox
document.getElementById('deleteToggle').addEventListener('change', toggleDeleteButtons);

// Ensure the form does not submit traditionally, and call createRepo instead
document.getElementById('repoForm').addEventListener('submit', function(event) {
    event.preventDefault();
    createRepo();
});
